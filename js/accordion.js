(function ($) {
  Drupal.behaviors.accordion = {
    attach: function (context, settings) {
      once('accordion-processed', '.paragraph--type--accordion-item', context).forEach(function (e, i) {
        let $accordionItem = $(e);
        let $title = $accordionItem.find('.field--name-field-sec-title');
        $title[0].addEventListener('click', function () {
          let $accordionContainer = $(this).closest('.field--name-field-accordion');
          let $parent = $(this).closest('.paragraph--type--accordion-item');

          if (!$parent.hasClass('active')) {
            $accordionContainer.find('.paragraph--type--accordion-item.active').removeClass('active').find('.field--name-field-sec-text').slideUp();
            $(this).siblings('.field--name-field-sec-text').slideDown();
            $parent.addClass('active');
          }
          else {
            $(this).siblings('.field--name-field-sec-text').slideUp();
            $parent.removeClass('active');
          }
        });
      });
    }
  };
})(jQuery);
