<?php

namespace Drupal\se_language_switcher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Language switcher settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'se_language_switcher_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['se_language_switcher.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['word_length'] = [
      '#type' => 'select',
      '#options' => [
        '0' => 'Whole word',
        '2' => '2',
        '3' => '3',
      ],
      '#title' => $this->t('Word length'),
      '#description' => $this->t('Select how long the language word should be. Eg. if you select 3 you get "Eng".'),
      '#default_value' => $this->config('se_language_switcher.settings')->get('word_length'),
    ];

    $form['active_tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Active tag'),
      '#description' => $this->t('The HTML tag container for the active language. Eg. h2, span, div.'),
      '#default_value' => $this->config('se_language_switcher.settings')->get('active_tag'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('se_language_switcher.settings')
      ->set('word_length', $form_state->getValue('word_length'))
      ->save();

    $this->config('se_language_switcher.settings')
      ->set('active_tag', $form_state->getValue('active_tag'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
