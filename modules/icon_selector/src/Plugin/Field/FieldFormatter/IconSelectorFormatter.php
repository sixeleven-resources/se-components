<?php

namespace Drupal\icon_selector\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'iconselector' formatter.
 *
 * @FieldFormatter(
 *   id = "iconselector_formatter",
 *   module = "icon_selector",
 *   label = @Translation("Icon Selector"),
 *   field_types = {
 *     "iconselector"
 *   }
 * )
 */
class IconSelectorFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $config         = \Drupal::config('icon_selector.settings');

    $paths = [];
    if ($config->get('hide_default') == NULL || $config->get('hide_default') == FALSE) {
      $path_to_module = \Drupal::service('extension.list.module')->getPath('icon_selector');
      $paths['default'] = DRUPAL_ROOT . '/' . $path_to_module . '/icons';
    }
    $config_icons = [];
    if ($config->get('icons_path') !== NULL) {
      if (str_contains($config->get('icons_path'), 'module:')) {
        $exploded_path         = explode(':', $config->get('icons_path'));
        $path_to_custom_module = \Drupal::service('extension.list.module')
          ->getPath($exploded_path[1]);

        $paths['custom_module'] = DRUPAL_ROOT . '/' . $path_to_custom_module . '/' . $exploded_path[2];
      }
      else {
        $config_icons_path_raw = \Drupal::service('file_system')
          ->realpath($config->get('icons_path'));
        $paths['public'] = $config_icons_path_raw;
      }
    }

    $elements   = [];
    foreach ($paths as $path) {
      $tree = $this->recursiveDirScan($path);
      $this->getIcon($items, $path, $tree, $elements);
    }
    return $elements;
  }

  /**
   * Get the icon to print.
   */
  public function getIcon($items, $icons_path, $tree, &$elements) {
    if (is_array($tree)) {
      foreach ($items as $delta => $item) {
        $exploded = explode('|', $item->value);

        if (isset($tree[$exploded[0]]) and isset($tree[$exploded[0]][$exploded[1]])) {
          $icon_url         = $tree[$exploded[0]][$exploded[1]]['file_url'];
          $encoded_url      = $this->fileUrl($icon_url);
          $icon_content     = file_get_contents($encoded_url);
          $elements[$delta] = [
            '#theme'        => 'icon_selector_element',
            '#icon_content' => $icon_content,
            '#attributes'   => [
              'id'    => 'icon-selector',
              'class' => [
                'icon-selector-element',
              ],
            ],
          ];
          return $elements;
        }
      }
    }

    return $elements;
  }

  /**
   * Get directory tree.
   */
  public function recursiveDirScan($path, $tree = []) {
    global $base_url;
    foreach (scandir($path) as $filename) {
      if ($filename[0] === '.') {
        continue;
      }

      $filePath = $path . '/' . $filename;
      if (is_dir($filePath)) {
        if (!isset($tree[$filename])) {
          $filename = str_replace(' ', '_', strtolower($filename));
          $tree[$filename] = [];
        }

        $tree[$filename] = $this->recursiveDirScan($filePath, $tree[$filename]);
      }
      else {
        $tree[str_replace('.svg', '', $filename)] = [
          'filename' => $filename,
          'file_url' => $base_url . str_replace(DRUPAL_ROOT, '', $filePath),
        ];
      }
    }

    return $tree;
  }

  /**
   *
   */
  public function fileUrl($url) {
    $parts = parse_url($url);
    $path_parts = array_map('rawurldecode', explode('/', $parts['path']));

    return $parts['scheme'] . '://' . $parts['host'] . implode('/', array_map('rawurlencode', $path_parts));
  }

}
