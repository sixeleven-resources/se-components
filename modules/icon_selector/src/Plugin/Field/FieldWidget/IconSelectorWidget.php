<?php

namespace Drupal\icon_selector\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use function PHPUnit\Framework\stringContains;

/**
 * Plugin implementation of the 'Iconselector' widget.
 *
 * @FieldWidget(
 *   id = "iconselector_widget",
 *   module = "icon_selector",
 *   label = @Translation("Icon Selector"),
 *   field_types = {
 *     "iconselector"
 *   }
 * )
 */
class IconSelectorWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $config         = \Drupal::config('icon_selector.settings');

    $default_icons = [];
    $item_values   = $items->getValue();
    $default_value = $item_values[0]['value'] ?? NULL;

    if ($config->get('hide_default') == NULL || $config->get('hide_default') == FALSE) {
      $path_to_module = \Drupal::service('extension.list.module')->getPath('icon_selector');
      $default_icons_path = DRUPAL_ROOT . '/' . $path_to_module . '/icons';
      $default_icons      = $this->getIcons($default_icons_path);
    }
    $config_icons = [];
    if ($config->get('icons_path') !== NULL) {
      if (str_contains($config->get('icons_path'), 'module:')) {
        $exploded_path = explode(':', $config->get('icons_path'));
        $path_to_custom_module = \Drupal::service('extension.list.module')->getPath($exploded_path[1]);

        $config_icons_path = DRUPAL_ROOT . '/' . $path_to_custom_module . '/' . $exploded_path[2];
      }
      else {
        $config_icons_path_raw = \Drupal::service('file_system')
          ->realpath($config->get('icons_path'));
        $config_icons_path     = $config_icons_path_raw;
      }
      $config_icons          = $this->getIcons($config_icons_path);
    }

    $options = array_merge($default_icons, $config_icons);
    if (!empty($options)) {
      $icons       = [];
      $icons_files = [];
      foreach ($options as $key => $value) {
        $this->sortOptionsArray($options, $key, $icons, $icons_files);
        if (isset($options[$key]["folder"])) {
          $processed_options['folders'][$key] = $options[$key]["folder"];
        }
      }

      $processed_options['icons'] = $icons;
      $processed_options['icons_files'] = $icons_files;

      $element['value'] = $element + [
        '#type'          => 'select',
        '#options'       => $processed_options['icons'],
        '#empty_value'   => '',
        '#default_value' => $default_value,
        '#description'   => t('Select an icon'),
        '#attached'      => [
          'library' => [
            'icon_selector/chosen',
            'icon_selector/icon-selector',
          ],
        ],
      ];

      $element['icon-selector-widget'] = [
        '#theme'      => 'icon_selector_widget',
        '#folders'      => $processed_options['folders'],
        '#icons'      => $processed_options['icons_files'],
        '#attributes' => [
          'id'    => 'icon-selector-widget-wrapper',
          'class' => [
            'icon-selector-widget-wrapper',
          ],
        ],
      ];
    }

    return $element;
  }

  /**
   *
   */
  public function getIcons($path) {
    $options = $this->recursiveDirScan($path);

    if (isset($options['icons'])) {
      $options['Other']['icons']       = $options['icons'];
      $options['Other']['icons_files'] = $options['icons_files'];
      unset($options['icons']);
      unset($options['icons_files']);
    }

    return $options;
  }

  /**
   *
   */
  public function recursiveDirScan($path, $tree = []) {
    global $base_url;
    foreach (scandir($path) as $filename) {
      if ($filename[0] === '.') {
        continue;
      }

      $filePath = $path . '/' . $filename;
      if (is_dir($filePath)) {
        if (!isset($tree[$filename])) {
          $filename = str_replace(' ', '_', strtolower($filename));
          $tree[$filename] = [];
        }

        $tree[$filename] = $this->recursiveDirScan($filePath, $tree[$filename]);
      }
      else {
        $exploded = explode('/', $path);

        $tree['folder'] = end($exploded);

        $foldername = str_replace(' ', '_', strtolower($tree['folder']));

        $tree['icons'][$foldername . '|' . str_replace('.svg', '', $filename)] = $filename;

        $file_url = $this->fileUrl($base_url . str_replace(DRUPAL_ROOT, '', $filePath));
        $tree['icons_files'][$foldername . '|' . str_replace('.svg', '', $filename)] = $file_url;
      }
    }

    return $tree;
  }

  /**
   *
   */
  public function sortOptionsArray($options, $key, &$icons, &$icons_files) {
    if (isset($options[$key]['icons'])) {
      asort($options[$key]['icons']);
      $icons[$key] = $options[$key]['icons'];
      asort($icons[$key]);
    }

    if (isset($options[$key]['icons_files'])) {
      asort($options[$key]['icons_files']);
      $icons_files[$key] = $options[$key]['icons_files'];
      asort($icons_files[$key]);
    }

    foreach ($options[$key] as $new_key => $value) {
      if ($new_key !== 'icons' and $new_key !== 'icons_files' and $new_key !== 'folder_name') {
        if (!isset($icons[$key])) {
          $icons[$key] = [];
        }

        if (!isset($icons_files[$key])) {
          $icons_files[$key] = [];
        }

        $this->sortOptionsArray($options[$key], $new_key, $icons[$key], $icons_files[$key]);
      }
    }
  }

  /**
   *
   */
  public function fileUrl($url) {
    $parts = parse_url($url);
    $path_parts = array_map('rawurldecode', explode('/', $parts['path']));

    return $parts['scheme'] . '://' . $parts['host'] . implode('/', array_map('rawurlencode', $path_parts));
  }

}
