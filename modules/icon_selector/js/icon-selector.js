/**
 * @file
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.icon_selector = {
    attach: function (context, settings) {
      var $icon_selector_thumbnails = $('.icon-selector--thumbnails');

      if ($icon_selector_thumbnails.length > 0) {
        var icon_selected = '.icon-selector--thumbnails-selected';

        $('.icon-selector-widget-wrapper').each(function (index) {
          if (!$(this).hasClass('initialized')) {
            var $icon_select = $(this).closest('.field--type-iconselector').find('select');

            var default_value = $icon_select.val();
            $icon_select.chosen();
            if (default_value && default_value !== '') {
              $(this).find('.icon-selector--thumbnails[data-filename="' + default_value + '"]').find('.icon-selector--thumbnails-wrapper').clone().appendTo($(this).find(icon_selected));
              $(this).addClass('initialized');
            }
          }
        });

        $icon_selector_thumbnails.off('click').on('click', function () {
          var $parent = $(this).closest('.icon-selector-widget-wrapper');
          var $parentGroup = $(this).closest('.icon-group');
          var groupName = $parentGroup.data('group');
          var filename = $(this).data('filename');

          $parent.find(icon_selected).empty();
          $parentGroup.find('.icon-selector--thumbnails[data-filename="' + filename + '"]').find('.icon-selector--thumbnails-wrapper').clone().appendTo($parent.find(icon_selected));
          var $icon_select = $(this).closest('.field--type-iconselector').find('select');
          var $selectGroup = $icon_select.find('optgroup[label="' + groupName + '"]');

          $icon_select.find('option').attr('selected', null);
          $selectGroup.find('option[value="' + filename + '"]').attr('selected', 'selected');
          $icon_select.trigger('chosen:updated');
        });

        $('.field--type-iconselector').find('select').off('change').on('change', function (e, selected) {
          var $iconWidget = $(this).closest('.field--type-iconselector').find('.icon-selector-widget-wrapper');
          var groupName = $(this)[0].selectedOptions[0].parentNode.getAttribute("label");
          var filename = selected.selected;

          $iconWidget.find(icon_selected).empty();
          $iconWidget.find('.icon-group-' + groupName).find('.icon-selector--thumbnails[data-filename="' + filename + '"]').find('.icon-selector--thumbnails-wrapper').clone().appendTo($iconWidget.find(icon_selected));

          $(this).find('option').attr('selected', null);
          $iconWidget.find('.icon-group-' + groupName).find('option[value="' + filename + '"]').attr('selected', 'selected');
        });

      }

      $('.icon-group h3').off().on('click', function () {
        $(this).siblings('.icons-wrapper').slideToggle('slow');
      });
    }
  };

})(jQuery, Drupal);
