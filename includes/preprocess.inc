<?php

/**
 * @file
 * Prepocess functions for defined paragraph.
 */

/**
 * Implements hook_preprocess_paragraph().
 */
function se_components_preprocess_paragraph(&$variables) {
  $paragraph = $variables['elements']['#paragraph'];
  $paragraphType = $paragraph->getType();
  $paragraphCallback = 'se_components_' . $paragraphType . '_callback';

  if (function_exists($paragraphCallback) === TRUE) {
    call_user_func_array($paragraphCallback, [&$variables]);
  }
}

/**
 * Callback for sec_content_image paragraph.
 *
 * @param mixed &$variables
 *   The element variables.
 */
function se_components_sec_content_image_callback(&$variables) {
  $variables['#attached']['library'][] = 'se_components/alignment';
  $paragraph = $variables['elements']['#paragraph'];
  $alignment = $paragraph->hasField('field_sec_alignment')
    ? $paragraph->get('field_sec_alignment')->value
    : NULL;
  if (!is_null($alignment)) {
    $variables["attributes"]['class'][] = 'alignment-' . $alignment;
  }
  $style = $paragraph->hasField('field_sec_style')
    ? $paragraph->get('field_sec_style')->value
    : NULL;
  if (!is_null($style)) {
    $variables["attributes"]['class'][] = 'style-' . $style;
  }
}

/**
 * Callback for accordion paragraph.
 *
 * @param mixed &$variables
 *   The element variables.
 */
function se_components_accordion_callback(&$variables) {
  $variables['#attached']['library'][] = 'se_components/accordion';
}
