# SE Components

### Install instructions

Add the following repo to you root composer.json file after the official `https://packages.drupal.org/8`

```
"repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "vcs",
            "url": "https://gitlab.com/sixeleven-resources/se-components.git"
        }
    ],
```

Install the module with
```
composer require 'sixeleven-resources/se-components:dev-main'
```
